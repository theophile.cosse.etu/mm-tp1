from pyecore.resources import ResourceSet
import filesystem  # pour le méta-modèle simplejava
import simplejava

rsetfs = ResourceSet()
rsetfs.metamodel_registry[filesystem.nsURI] = filesystem  # enregistre le méta-modèle simplejava
rsetsj = ResourceSet()
rsetsj.metamodel_registry[simplejava.nsURI] = simplejava  # enregistre le méta-modèle simplejava

resourcefs = rsetfs.get_resource('../filesystem/model/Root.xmi')
racinefs = resourcefs.contents[0]
resourcesj = rsetsj.get_resource('../simplejava/model/JProject.xmi')
racinesj = resourcesj.contents[0]




#def spam(ecls):
#    eclass = ecls.eClass
#    pack = eclass.ePackage
#    for classif in pack.eClassifiers:
#        if isintance()


def count_meta_classes(ecls):
    return len(ecls.ePackage.eClassifiers)

def list_super_meta_classes(ecls):
    res = []
    for c in ecls.ePackage.eClassifiers:
        if ecls in c.eAllSuperTypes():
            res.append(c)
    return res

#print(count_meta_classes(racinefs.eClass))
#print(list_super_meta_classes(simplejava.JClassifier.eClass))


#for att in simplejava.JClassifier.eClass.eAllAttributes():
#    print(dir(att))
