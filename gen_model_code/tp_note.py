from pyecore.resources import ResourceSet
import json

id_dict={}
curr_id=0

def serialize_mm(pack,filename):
    p = pack
    mm_dict= {'name':p.name,'eClassifiers' : []}
    for classi in p.eClassifiers:
        mm_dict['eClassifiers'].append(seri_obj(classi))

    for classi in  mm_dict['eClassifiers']:
        fix_ref(classi)

    jsonfile = open(filename,"w")
    jsonfile.write(json.dumps(mm_dict))
    jsonfile.close()

def fix_ref(classi_dict):
    if classi_dict['parent']:
        classi_dict['parent'] = id_dict[classi_dict['parent']]
    for ref in classi_dict['references']:
        ref['_id'] = id_dict[ref['_id']]
    
def seri_obj(classi):
    global curr_id
    res_dict = {'_id' : curr_id,
                'uri' : classi.ePackage.nsURI + '#' + classi.name,
                'name' : classi.name,
                'parent' : next(iter(classi.eAllSuperTypes()),None),
                'attributes' : [],
                'references' : []}
    for attrib in  classi.eAllAttributes():
        res_dict['attributes'].append({'name' : attrib.name,
                                       'eType' : attrib.eType.name,
                                       'defaultValue' : attrib.get_default_value()}) 
    for ref in  classi.eAllReferences():
        if ref.containment:
            res_dict['references'].append({'_id' : ref.eType,
                                        'lowerBound' : ref.lowerBound,
                                        'upperBound' : ref.upperBound,
                                        'eOpposite' : ref.eOpposite,
                                        'containment' : ref.containment})    
    
    id_dict[classi] = curr_id
    curr_id +=1

    return res_dict

def test():
    rset = ResourceSet()
    resource = rset.get_resource('../filesystem/model/filesystem.ecore')
    pack = resource.contents[0]
    rset.metamodel_registry[pack.nsURI] = pack

    rset2 = ResourceSet()
    resource2 = rset2.get_resource('../simplejava/model/simplejava.ecore')
    pack2 = resource2.contents[0]
    rset2.metamodel_registry[pack2.nsURI] = pack2

    serialize_mm(pack,"exemple_fs.json")
    serialize_mm(pack2,"exemple_java.json")

#test()

