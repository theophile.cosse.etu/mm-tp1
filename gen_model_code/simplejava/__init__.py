
from .simplejava import getEClassifier, eClassifiers
from .simplejava import name, nsURI, nsPrefix, eClass
from .simplejava import JClass, JClassifier, JMethod, JAttribute, JType, JInheritance, JInterface, JReference, JInvocation, JPackage, JPrimitiveType, JProject, JInteger, JBoolean, JDouble


from . import simplejava

__all__ = ['JClass', 'JClassifier', 'JMethod', 'JAttribute', 'JType', 'JInheritance', 'JInterface',
           'JReference', 'JInvocation', 'JPackage', 'JPrimitiveType', 'JProject', 'JInteger', 'JBoolean', 'JDouble']

eSubpackages = []
eSuperPackage = None
simplejava.eSubpackages = eSubpackages
simplejava.eSuperPackage = eSuperPackage

JClass.jattribute.eType = JAttribute
JClassifier.jmethod.eType = JMethod
JAttribute.jtype.eType = JType
JInheritance.sub.eType = JClass
JInheritance.ssuper.eType = JClassifier
JInheritance.superI.eType = JInterface
JReference.jmethod.eType = JMethod
JReference.jclassifier.eType = JClassifier
JInvocation.cible.eType = JMethod
JInvocation.source.eType = JMethod
JPackage.jclassifier.eType = JClassifier
JProject.jpackage.eType = JPackage
JProject.jprimitivetype.eType = JPrimitiveType
JProject.jinheritance.eType = JInheritance

otherClassifiers = []

for classif in otherClassifiers:
    eClassifiers[classif.name] = classif
    classif.ePackage = eClass

for classif in eClassifiers.values():
    eClass.eClassifiers.append(classif.eClass)

for subpack in eSubpackages:
    eClass.eSubpackages.append(subpack.eClass)
