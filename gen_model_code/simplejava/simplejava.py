"""Definition of meta model 'simplejava'."""
from functools import partial
import pyecore.ecore as Ecore
from pyecore.ecore import *


name = 'simplejava'
nsURI = 'http://www.example.org/simplejava'
nsPrefix = 'simplejava'

eClass = EPackage(name=name, nsURI=nsURI, nsPrefix=nsPrefix)

eClassifiers = {}
getEClassifier = partial(Ecore.getEClassifier, searchspace=eClassifiers)


class JMethod(EObject, metaclass=MetaEClass):

    name = EAttribute(eType=EString, unique=True, derived=False, changeable=True)

    def __init__(self, *, name=None):
        # if kwargs:
        #    raise AttributeError('unexpected arguments: {}'.format(kwargs))

        super().__init__()

        if name is not None:
            self.name = name


class JAttribute(EObject, metaclass=MetaEClass):

    name = EAttribute(eType=EString, unique=True, derived=False, changeable=True)
    jtype = EReference(ordered=True, unique=True, containment=False, derived=False)

    def __init__(self, *, jtype=None, name=None):
        # if kwargs:
        #    raise AttributeError('unexpected arguments: {}'.format(kwargs))

        super().__init__()

        if name is not None:
            self.name = name

        if jtype is not None:
            self.jtype = jtype


@abstract
class JType(EObject, metaclass=MetaEClass):

    def __init__(self):
        # if kwargs:
        #    raise AttributeError('unexpected arguments: {}'.format(kwargs))

        super().__init__()


class JInheritance(EObject, metaclass=MetaEClass):

    sub = EReference(ordered=True, unique=True, containment=False, derived=False, upper=-1)
    ssuper = EReference(ordered=True, unique=True, containment=False, derived=False)
    superI = EReference(ordered=True, unique=True, containment=False, derived=False, upper=-1)

    def __init__(self, *, sub=None, ssuper=None, superI=None):
        # if kwargs:
        #    raise AttributeError('unexpected arguments: {}'.format(kwargs))

        super().__init__()

        if sub:
            self.sub.extend(sub)

        if ssuper is not None:
            self.ssuper = ssuper

        if superI:
            self.superI.extend(superI)


class JReference(EObject, metaclass=MetaEClass):

    jmethod = EReference(ordered=True, unique=True, containment=False, derived=False)
    jclassifier = EReference(ordered=True, unique=True, containment=False, derived=False)

    def __init__(self, *, jmethod=None, jclassifier=None):
        # if kwargs:
        #    raise AttributeError('unexpected arguments: {}'.format(kwargs))

        super().__init__()

        if jmethod is not None:
            self.jmethod = jmethod

        if jclassifier is not None:
            self.jclassifier = jclassifier


class JInvocation(EObject, metaclass=MetaEClass):

    cible = EReference(ordered=True, unique=True, containment=False, derived=False)
    source = EReference(ordered=True, unique=True, containment=False, derived=False)

    def __init__(self, *, cible=None, source=None):
        # if kwargs:
        #    raise AttributeError('unexpected arguments: {}'.format(kwargs))

        super().__init__()

        if cible is not None:
            self.cible = cible

        if source is not None:
            self.source = source


class JPackage(EObject, metaclass=MetaEClass):

    jclassifier = EReference(ordered=True, unique=True, containment=True, derived=False, upper=-1)

    def __init__(self, *, jclassifier=None):
        # if kwargs:
        #    raise AttributeError('unexpected arguments: {}'.format(kwargs))

        super().__init__()

        if jclassifier:
            self.jclassifier.extend(jclassifier)


class JProject(EObject, metaclass=MetaEClass):

    jpackage = EReference(ordered=True, unique=True, containment=True, derived=False, upper=-1)
    jprimitivetype = EReference(ordered=True, unique=True,
                                containment=True, derived=False, upper=-1)
    jinheritance = EReference(ordered=True, unique=True, containment=True, derived=False, upper=-1)

    def __init__(self, *, jpackage=None, jprimitivetype=None, jinheritance=None):
        # if kwargs:
        #    raise AttributeError('unexpected arguments: {}'.format(kwargs))

        super().__init__()

        if jpackage:
            self.jpackage.extend(jpackage)

        if jprimitivetype:
            self.jprimitivetype.extend(jprimitivetype)

        if jinheritance:
            self.jinheritance.extend(jinheritance)


@abstract
class JClassifier(JType):

    name = EAttribute(eType=EString, unique=True, derived=False, changeable=True)
    jmethod = EReference(ordered=True, unique=True, containment=True, derived=False, upper=-1)

    def __init__(self, *, jmethod=None, name=None, **kwargs):

        super().__init__(**kwargs)

        if name is not None:
            self.name = name

        if jmethod:
            self.jmethod.extend(jmethod)


@abstract
class JPrimitiveType(JType):

    def __init__(self, **kwargs):

        super().__init__(**kwargs)


class JClass(JClassifier):

    isAbstract = EAttribute(eType=EBoolean, unique=True, derived=False, changeable=True)
    jattribute = EReference(ordered=True, unique=True, containment=True, derived=False, upper=-1)

    def __init__(self, *, jattribute=None, isAbstract=None, **kwargs):

        super().__init__(**kwargs)

        if isAbstract is not None:
            self.isAbstract = isAbstract

        if jattribute:
            self.jattribute.extend(jattribute)


class JInterface(JClassifier):

    def __init__(self, **kwargs):

        super().__init__(**kwargs)


class JInteger(JPrimitiveType):

    def __init__(self, **kwargs):

        super().__init__(**kwargs)


class JBoolean(JPrimitiveType):

    def __init__(self, **kwargs):

        super().__init__(**kwargs)


class JDouble(JPrimitiveType):

    def __init__(self, **kwargs):

        super().__init__(**kwargs)
