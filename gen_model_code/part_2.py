from pyecore.resources import ResourceSet
import filesystem  # pour le méta-modèle simplejava

rset = ResourceSet()
rset.metamodel_registry[filesystem.nsURI] = filesystem  # enregistre le méta-modèle simplejava


resource = rset.get_resource('../filesystem/model/Root.xmi')
racine = resource.contents[0]

#Q1
def number_of_file(d):
    c = 0
    if isinstance(d,filesystem.File):
        return 1
    for af in d.abstractfile:
        if isinstance(af,filesystem.File):
            c+=1
        else:
            c+=number_of_file(af)
    return c

#Q2
# retourn 0 car le model choisi a toute les size à 0
def total_weight(d):
    c = 0
    if isinstance(d,filesystem.File):
        return d.size
    for af in d.abstractfile:
        if isinstance(af,filesystem.File):
            c+=af.size
        else:
            c+=total_weight(af)
    return c
#Q3
#meme que Q2 



#print(number_of_file(racine))
#print(total_weight(racine))
