from pyecore.resources import ResourceSet

rset = ResourceSet()
resource = rset.get_resource('../filesystem/model/filesystem.ecore')
pack = resource.contents[0]
rset.metamodel_registry[pack.nsURI] = pack

rset2 = ResourceSet()
resource2 = rset2.get_resource('../simplejava/model/simplejava.ecore')
pack2 = resource2.contents[0]
rset2.metamodel_registry[pack2.nsURI] = pack2

#print(dir(pack))

def get_meta_class(name):
    return pack.getEClassifier(name)

#print(get_meta_class('abc'))
#print(get_meta_class('File'))

def create_meta_class(name,package):
    return package.getEClassifier(name)()

#v1 Pas bon
def set_name(obj, name):
    for att in obj.eClass.eAllAttributes():
        if att.name == "name":
            obj.name = name
            return
#v2
def set_name_2(obj, name):
    nameF = obj.eClass.findEStructuralFeature('name')
    if nameF:
        obj.eSet(nameF,name)

my_file_2 = create_meta_class("File",pack)
set_name_2(my_file_2, "aaaj")
#print(my_file_2.eGet('name'))



my_file = create_meta_class("File",pack)
my_dir = create_meta_class("Directory",pack)
my_class = create_meta_class("JClass",pack2)

set_name(my_file, "test")
set_name(my_class, "test2")   


#print(my_file.name)
#print(my_class.name)

#print(pack.getEClassifier('Directory').eStructuralFeatures) 
#print(pack.getEClassifier('File').eStructuralFeatures)
#print(pack.getEClassifier('Directory').eAllAttributes())
#print(pack.getEClassifier('File').eAllAttributes())

#print(my_dir.eClass.eStructuralFeatures) # Pourquoi resultat différent eStructuralFeatures / eAllAttributes
#print(my_file.eClass.eStructuralFeatures) # Conclusion -> eStructuralFeatures ne contient pas les attribut hérité mais eAllAttributes contient uniquement les attributs ( pas de ref)
#print(my_dir.eClass.eAllAttributes())
#print(my_file.eClass.eAllAttributes())


# Derniere partie

def list_attribute_name_and_ref(obj):
    print("struct feature :")
    for structFeat in obj.eClass.eAllReferences():
        print(structFeat.name + " = " + str(obj.eGet(structFeat)))
    print("all attributes :")
    for attrib in obj.eClass.eAllAttributes():
        print(attrib.name + " = " + str(obj.eGet(attrib)))   



my_file.name= "readme"
my_file.extension = ".md"
my_file.size = 4000

#my_dir.name = "home"
#my_dir.abstractfile.append(my_file) # Pourquoi append my_file detruit la ref ?


#list_attribute_name_and_ref(my_file)

def transform_string(obj):
    for structFeat in obj.eClass.eStructuralFeatures:
        #if isinstance(structFeat.eType,pyecore.ecore.EString):
        if structFeat.eType.name == "EString":
            old_val = obj.eGet(structFeat)
            obj.eSet(structFeat,"id"+old_val)
            return



#transform_string(my_file)
#list_attribute_name_and_ref(my_file)

#print(get_meta_class('File').ePackage.nsURI)


print("##########all contents")
for a in get_meta_class('File').eAllContents():
    #print(dir(a))
    print(a)
print("#########attributes")
for a in get_meta_class('File').eAllAttributes():
    #print(dir(a))
    print(a)
print("##########references")
for a in get_meta_class('File').eAllReferences():
    #print(dir(a))
    print(a)


"""
for a in get_meta_class('Directory').eAllReferences():
    print(a)
    break 
"""