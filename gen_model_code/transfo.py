
import pyecore.ecore as ecore
from pyecore.resources import ResourceSet
import motra
from motra import m2m

import simplejava  # pour le méta-modèle simplejava


##
# First simple in-place transformation
##
# "changename" is an in-place transformation
# inputs and outputs are the same
to_upper_case = m2m.Transformation('to_upper_case',
                                inputs=['in_model'],
                                outputs=['in_model'])


# First parameter of any mapping *must* be named "self"
# typing is mandatory
@to_upper_case.mapping
def update_name(self: simplejava.JClass):
    self.name = self.name.upper()

# parameter names must be the same as the one declared in the signature above
@to_upper_case.main
def main(in_model, ggg):
    print(in_model, ggg)
    for element in motra.objects_of_kind(in_model, simplejava.JClass):
        update_name(element)



res = to_upper_case.run(in_model='../simplejava/model/JProject.xmi', ggg=45)
res.outputs[0].save(output="res.xmi")


##
# First simple in-place transformation
##
# "changename" is an in-place transformation
# inputs and outputs are the same
add_prefix = m2m.Transformation('add_prefix',
                                inputs=['in_model'],
                                outputs=['in_model'])


# First parameter of any mapping *must* be named "self"
# typing is mandatory
@add_prefix.mapping
def add_prefix_to_name(self: simplejava.JClassifier):
    self.name = str(id(self.name)) + self.name

# parameter names must be the same as the one declared in the signature above
@add_prefix.main
def main2(in_model):
    for element in motra.objects_of_kind(in_model, simplejava.JClassifier):
        add_prefix_to_name(element)

res2 = add_prefix.run(in_model='../simplejava/model/JProject.xmi')
res2.outputs[0].save(output="res2.xmi")



##
# First simple in-place transformation
##
# "changename" is an in-place transformation
# inputs and outputs are the same
add_prop = m2m.Transformation('add_prop',
                                inputs=['in_model'],
                                outputs=['in_model'])


# First parameter of any mapping *must* be named "self"
# typing is mandatory
@add_prop.mapping
def add_prop_to_class(self: simplejava.JClassifier):
    self.eClass.eStructuralFeatures.append(EAttribute('id', EString))
    self.id="kjl"

# parameter names must be the same as the one declared in the signature above
@add_prop.main
def main3(in_model):
    for element in motra.objects_of_kind(in_model, simplejava.JClassifier):
        add_prop_to_class(element)

res3 = add_prop.run(in_model='../simplejava/model/JProject.xmi')
res3.outputs[0].save(output="res3.xmi")