from pyecore.resources import ResourceSet
import simplejava  # pour le méta-modèle simplejava

rset = ResourceSet()
rset.metamodel_registry[simplejava.nsURI] = simplejava  # enregistre le méta-modèle simplejava


resource = rset.get_resource('../simplejava/model/JProject.xmi')
racine = resource.contents[0]

#Q1
def list_class(project):
    for p in project.jpackage:
        for c in p.jclassifier:
            if isinstance(c,simplejava.JClass):
                print(c.name)

#Q2
def list_attrib(project):
    for p in project.jpackage:
        for c in p.jclassifier:
            if isinstance(c,simplejava.JClass):
                for attrib in c.jattribute:    
                    print(attrib.name)  

#Q3
def how_deep_is_inheritance(jclass):
    for i in racine.jinheritance:
        for s in i.sub:
            if s == jclass:
                return how_deep_is_inheritance(i.ssuper)+1
    return 0



#Q4
#Mon MM ne permet pas la creation de package imbriqué
def full_class_name(jclass): 
    for p in racine.jpackage:
        if jclass in p.jclassifier:
            return jclass.name + str(p)


#list_class(racine)

#list_attrib(racine)

#print(racine.jpackage[0].jclassifier[0].eContainer().eContainer())

'''
for p in racine.jpackage:
    for c in p.jclassifier:
        if isinstance(c,simplejava.JClass):
            print(c.name + " " + str(how_deep_is_inheritance(c)))
'''

'''
for p in racine.jpackage:
    for c in p.jclassifier:
        if isinstance(c,simplejava.JClass):
            print(full_class_name(c))
'''
