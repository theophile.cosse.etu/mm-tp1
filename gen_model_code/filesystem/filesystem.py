"""Definition of meta model 'filesystem'."""
from functools import partial
import pyecore.ecore as Ecore
from pyecore.ecore import *


name = 'filesystem'
nsURI = 'http://filesystem/1.0'
nsPrefix = 'filesystem'

eClass = EPackage(name=name, nsURI=nsURI, nsPrefix=nsPrefix)

eClassifiers = {}
getEClassifier = partial(Ecore.getEClassifier, searchspace=eClassifiers)


@abstract
class AbstractFile(EObject, metaclass=MetaEClass):

    name = EAttribute(eType=EString, unique=True, derived=False, changeable=True)

    def __init__(self, *, name=None):
        # if kwargs:
        #    raise AttributeError('unexpected arguments: {}'.format(kwargs))

        super().__init__()

        if name is not None:
            self.name = name


class Root(EObject, metaclass=MetaEClass):

    abstractfile = EReference(ordered=True, unique=True, containment=True, derived=False, upper=-1)

    def __init__(self, *, abstractfile=None):
        # if kwargs:
        #    raise AttributeError('unexpected arguments: {}'.format(kwargs))

        super().__init__()

        if abstractfile:
            self.abstractfile.extend(abstractfile)


class Directory(AbstractFile):

    abstractfile = EReference(ordered=True, unique=True, containment=True, derived=False, upper=-1)

    def __init__(self, *, abstractfile=None, **kwargs):

        super().__init__(**kwargs)

        if abstractfile:
            self.abstractfile.extend(abstractfile)


class File(AbstractFile):

    extension = EAttribute(eType=EString, unique=True, derived=False, changeable=True)
    size = EAttribute(eType=EInt, unique=True, derived=False, changeable=True)

    def __init__(self, *, extension=None, size=None, **kwargs):

        super().__init__(**kwargs)

        if extension is not None:
            self.extension = extension

        if size is not None:
            self.size = size


class Link(File):

    abstractfile = EReference(ordered=True, unique=True, containment=False, derived=False)

    def __init__(self, *, abstractfile=None, **kwargs):

        super().__init__(**kwargs)

        if abstractfile is not None:
            self.abstractfile = abstractfile
