
from .filesystem import getEClassifier, eClassifiers
from .filesystem import name, nsURI, nsPrefix, eClass
from .filesystem import Directory, AbstractFile, File, Link, Root


from . import filesystem

__all__ = ['Directory', 'AbstractFile', 'File', 'Link', 'Root']

eSubpackages = []
eSuperPackage = None
filesystem.eSubpackages = eSubpackages
filesystem.eSuperPackage = eSuperPackage

Directory.abstractfile.eType = AbstractFile
Link.abstractfile.eType = AbstractFile
Root.abstractfile.eType = AbstractFile

otherClassifiers = []

for classif in otherClassifiers:
    eClassifiers[classif.name] = classif
    classif.ePackage = eClass

for classif in eClassifiers.values():
    eClass.eClassifiers.append(classif.eClass)

for subpack in eSubpackages:
    eClass.eSubpackages.append(subpack.eClass)
